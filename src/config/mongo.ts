import { Config } from '@nodearch/config';


const mongoose = {
  connectionString: Config.env('MONGODB_URL', { defaults: { all: 'mongodb://localhost/archDB' } }),
  mongooseOptions: {
    useNewUrlParser: true 
  } 
};


export { mongoose };