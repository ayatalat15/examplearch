import { Joi } from '@nodearch/rest';

export const createProduct = Joi.object({
  query: {
    num: Joi.number()
  },
  body: Joi.object({
    name: Joi.string().required(),
    price: Joi.number().required()
  }).required()
});