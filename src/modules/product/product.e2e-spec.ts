import { describe, it, before, beforeEach } from 'mocha';
import { expect } from 'chai';
import supertest from 'supertest';
import { ArchApp } from '@nodearch/core';
import { Mongoose, models, connection } from '@nodearch/mongoose';
import { ProductModule } from './product.module';
import { RestServer, express, ExpressMiddleware, RegisterRoutes, StartExpress, Sequence } from '@nodearch/rest';


describe('[e2e]Product', () => {

  let archApp: ArchApp;
  let restServer: RestServer;
  let request: supertest.SuperTest<supertest.Test>;

  before(async () => {
    archApp = new ArchApp(
      [
        ProductModule
      ],
      {
        logger: { error: () => { }, warn: () => { }, info: () => { }, debug: () => { } },
        extensions: [
          new Mongoose({ connectionString: 'mongodb://localhost/archDB', mongooseOptions: { useNewUrlParser: true } }),
          new RestServer(
            {
              config: {
                port: 3000,
                hostname: 'localhost',
                joiValidationOptions: {
                  abortEarly: false,
                  allowUnknown: true
                }
              },
              sequence: new Sequence([
                new ExpressMiddleware(express.json()),
                new ExpressMiddleware(express.urlencoded({ extended: false })),
                new RegisterRoutes(),
                new StartExpress()
              ])
            }
          )
        ]
      }
    );

    await archApp.load();

    restServer = archApp.getExtInstances<RestServer>(RestServer)[0];

    request = supertest(restServer.expressApp);
  });

  after(async () => {
    restServer.close();
    connection.close();
  });

  describe('ProductController', () => {

    beforeEach(async () => {
      await models['Product'].deleteMany({});
    });

    it('GET /products', async () => {

      await models['Product'].create(
        { name: 'C++ Book', price: 150 }
      );

      const res = await request
        .get('/products')
        .expect(200);

      expect(res.body).property('data');
      expect(res.body.data).length(1);
      expect(res.body.data[0].name).equals('C++ Book');
      expect(res.body.data[0].price).equals(150);
    });

    it('POST /products', async () => {
      await request
        .post('/products')
        .set('Content-Type', 'application/json')
        .send({ name: 'C# Book', price: 120 })
        .expect(200);

      const dbRes = await models['Product'].find({});

      expect(dbRes).to.not.be.null;
      expect(dbRes).length(1);
      expect(dbRes[0].name).to.equal('C# Book');
      expect(dbRes[0].price).to.equal(120);
    });
  })
});