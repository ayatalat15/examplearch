import { Post, Get, express, Put, Delete, Validate } from '@nodearch/rest';
import { Controller } from '@nodearch/core';
import { ProductService } from './product.service';
import { Product } from './product';
import { createProduct } from './validation-schema/create-product';


@Controller('products')
export class ProductController {

  private productService: ProductService;

  constructor(productService: ProductService) {
    this.productService = productService;
  }

  @Get('/:id')
  async getProduct(req: express.Request, res: express.Response) {
    const product: Product | undefined = await this.productService.getProduct(parseInt(req.params.id));
    res.json({ data: product });
  }

  @Get()
  async getProducts(req: express.Request, res: express.Response) {
    const products: Product[] = await this.productService.getProducts();
    res.json({ data: products });
  }

  @Validate(createProduct)
  @Post()
  async addProduct(req: express.Request, res: express.Response) {
    const productData: Product = <Product>req.body;
    const result = await this.productService.addProduct(productData);
    res.json({ result });
  }

  @Put('/:id')
  async updateProduct(req: express.Request, res: express.Response) {
    const productId = parseInt(req.params.id);
    const { name, price } = req.body;
    const result = await this.productService.updateProduct(productId, name, price);
    res.json({ result });
  }

  @Delete('/:id')
  async removeProduct(req: express.Request, res: express.Response) {
    const result = await this.productService.removeProduct(parseInt(req.params.id));
    res.json({ result });
  }
}