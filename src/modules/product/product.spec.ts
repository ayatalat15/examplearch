import { describe, it } from 'mocha';
import { expect } from 'chai';
import { ProductService } from './product.service';
import { ArchApp } from '@nodearch/core';
import { ProductModule } from './product.module';
import { ProductRepository } from './product.repository';


describe('[unit]ProductModule', () => {
  let archApp: ArchApp;

  before(async () => {
    archApp = new ArchApp(
      [
        ProductModule
      ],
      {
        logger: { error: () => { }, warn: () => { }, info: () => { }, debug: () => { } },
        overrideProviders: [
          {
            // Override Mongoose Model
            provider: ProductRepository,
            use: {
              create: () => { },
              find: () => { return [{ name: 'C++ Book', price: 150 }]; }
            }
          }
        ]
      }
    );

    await archApp.load();
  });

  describe('ProductService', () => {
    it('addProduct', async () => {
      const productService = archApp.get<ProductService>(ProductService);
      const res = await productService.getProducts();
      expect(res).to.deep.equals([{ name: 'C++ Book', price: 150 }]);
    });
  });
});
