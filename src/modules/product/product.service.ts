import { Injectable } from '@nodearch/core';
import { Product } from './product';
import { ProductRepository } from './product.repository';


@Injectable()
export class ProductService {

  private productModel: ProductRepository;

  constructor(productModel: ProductRepository) {
    this.productModel = productModel;
  }

  getProduct(id: number): Promise<Product | undefined> {
    return this.productModel.findOne({ _id: id });
  }

  getProducts(): Promise<Product[]> {
    return this.productModel.find({});
  }

  addProduct(product: Product): Promise<Product> {
    return this.productModel.create(product);
  }

  updateProduct(id: number, name: string, price: number): Promise<Product> {
    return this.productModel.update(id, { name, price });
  }

  removeProduct(id: number): any {
    return this.productModel.remove(id);
  }
}