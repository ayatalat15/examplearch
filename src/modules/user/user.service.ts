import { Injectable } from '@nodearch/core';
import { User } from './user';
import { UserRepository } from './user.repository';
import passport from 'passport';
import * as passportLocal from 'passport-local';


@Injectable()
export class UserService {
    private userModel: UserRepository;

    constructor(userModel: UserRepository) {
        this.userModel = userModel;

        console.log('user service')
        passport.use(new passportLocal.Strategy({
            usernameField: 'email',
            passwordField: 'password'
        },
            async (email: string, password: string, done: any) => {
                this.userModel.findOne({ email: email  }, function (err: any, user: User) {
                    if (err) { return done(err); }
                    if (!user) { return done(null, false); }
                    if (!user.password || user.password !== password) {
                        return done('password not correct')
                    }
                    return done(null, user);
                  });
            }

        ))
    }

    getUser(id: number): Promise<User | undefined> {
        return this.userModel.findOne(id)
    }

    getUsers(): Promise<User[]> {
        return this.userModel.find({})
    }

    addUser(user: User): Promise<User> {
        return this.userModel.create(user);
    }

    updateUser(id: number, user: User): Promise<User> {
        return this.userModel.update(id, user);
    }

    removeProduct(id: number): any {
        return this.userModel.delete(id);
    }
}