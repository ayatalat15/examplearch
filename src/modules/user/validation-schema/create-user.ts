import { Joi } from '@nodearch/rest';

export const createUser = Joi.object({
  body: Joi.object({
    name: Joi.string().required(),
    email: Joi.string().required(),
    password: Joi.string().required(),
    address: Joi.string(),
    phone: Joi.number(),
    dob: Joi.date()
  }).required()
});