import { Post, Get, express, Put, Delete, Validate, Middleware } from '@nodearch/rest';
import { Controller } from '@nodearch/core';
import { UserService } from './user.service';
import { User } from './user';
import { createUser } from './validation-schema/create-user';
import passport from 'passport';


@Controller('users')
export class UserController {
    private userService: UserService;

    constructor(userService: UserService) {
        this.userService = userService;
    }

    @Get('/:id')
    async getUser(req: express.Request, res: express.Response) {
        const user: User | undefined = await this.userService.getUser(parseInt(req.params.id));
        res.json({data: user});
    }

    @Get()
    async getUsers(req: express.Request, res: express.Response) {
        const users: User[] = await this.userService.getUsers();
        res.json({data: users});
    }

    @Validate(createUser)
    @Post()
    async addUser(req: express.Request, res: express.Response) {
        const userData: User = <User>req.body;
        const result = await this.userService.addUser(userData);
        res.json({data: result});
    }

    @Put('/:id')
    async updateUser(req: express.Request, res: express.Response) {
        const userId = parseInt(req.params.id);
        const user = req.body;
        const result = await this.userService.updateUser(userId, user);
        res.json({ result });
    }

    @Delete('/:id')
    async deleteUser (req: express.Request, res: express.Response) {
        const result = await this.userService.removeProduct(parseInt(req.params.id));
        res.json({ result });
    }

    @Post('/login/local')
    @Middleware([passport.authenticate('local', { session: false })])
    login(req: express.Request, res: express.Response) {
        res.send(req.user);
    }
}
