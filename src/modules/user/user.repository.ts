import { Schema, model } from '@nodearch/mongoose';
import { User } from './user';
import * as bcrypt from 'bcrypt';

const userSchema = new Schema({
    name: String,
    email: String,
    password: String, 
    address: String,
    phone: Number,
    dob: Date
})



const userModel = model<any>('User', userSchema);

function hashPassword(password: string) {
    return bcrypt.hashSync(password, 10);
}

userSchema.pre('save', function (next) {
    console.log('pre save')
    let user: any = this;
    // check if password is present and is modified.
    if (user.password && this.isModified('password') ) {
        user.password = hashPassword(user.password);
    }
  
    next();
  
  });

  

export class UserRepository {
    findOne(...args: any[]): Promise<User> {
        return userModel.findOne(...args).exec();
    }

    find(...args: any[]): Promise<User[]> {
        return userModel.find(...args).exec();
    }

    create(...docs: any[]): Promise<User> {
        return userModel.create(...docs);
    }

    update(id: number, user: User): Promise<User> {
        return userModel.update({_id: id}, user).exec();
    }

    delete(id: number) {
        return userModel.deleteOne({_id: id});
    }
}
