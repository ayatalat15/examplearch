import { Post, Get, express, Put, Delete, Validate, Middleware } from '@nodearch/rest';
import { Controller } from '@nodearch/core';

import passport  from 'passport';


const googleSuccessRedirect = 'http://localhost:3000/testSuccess';

const googleErrorRedirect = 'http://localhost:3000/testFailed';

@Controller('auth')
export class AuthController {
 

    @Get('login/google')
    @Middleware([passport.authenticate('google', { scope: ['profile', 'email'], session: false })])
    googleLogin() {}
  


  @Get('google/callback')
  @Middleware([passport.authenticate('google', { session: false })])
  googleLoginCallback(req: express.Request, res: express.Response): void {
      if (req.user) {
          res.redirect(googleSuccessRedirect);
      }
      else {
        res.redirect(googleErrorRedirect);
      }
  }


  @Get('/testSuccess')
  testSuccess(req: express.Request, res: express.Response): void{
      res.json('test success');
  }

  @Get('testFailed') 
  testFailed(req: express.Request, res: express.Response): void{
      res.json ('test failed');
  }

}