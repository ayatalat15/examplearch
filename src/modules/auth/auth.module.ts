import { Module } from '@nodearch/core'; 
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

@Module({
    imports: [],
    providers: [
        AuthService
    ],
    controllers: [AuthController],
    exports: []
})


export class AuthModule { }