import { Injectable } from '@nodearch/core';
import passport  from 'passport';
import * as passportGoogle from 'passport-google-oauth20';
import { auth } from '../../config/auth';
import { UserService } from '../user/user.service';



@Injectable()
export class AuthService {
    
    private userService : UserService;

    constructor(userService : UserService) {
        this.userService = userService;
        passport.use(new passportGoogle.Strategy({
            clientID: auth.googleAuth.clientID,
            clientSecret: auth.googleAuth.clientSecret,
            callbackURL: auth.googleAuth.callbackURL
          },
          async (accessToken, refreshToken, profile, done) => {
            const user = await this.checkUser(profile, done);

            return done('', {user});
          }
               
        ));
    
    }



    checkUser (profile: any, done: any) {
        const user = this.userService.getUser(profile.id);
        if(user) {
            return done(null, user);
        }
    }
}